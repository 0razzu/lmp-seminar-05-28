package service;


import model.Base;
import model.Derived;
import model.SomeExternalizableClass;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Comparator;
import java.util.function.Function;
import java.util.function.Predicate;

import static org.junit.jupiter.api.Assertions.*;


public class TestService {
    @Test
    void testGetInfo() {
        Base base0 = new Base();
        Base base1 = new Base("Some description");
        Base base2 = new Base("The most basic base");
        Base base3 = new Derived("Not just a base indeed", "Hey!");
        Derived derived0 = new Derived();
        Derived derived1 = new Derived("Only additional description has been provided");
        Derived derived2 = new Derived("Some description", "Some additional description");
        
        assertAll(
                () -> assertEquals(
                        Arrays.asList(
                                "Some standard description",
                                "Some description",
                                "The most basic base"
                        ),
                        Service.getInfo(Arrays.asList(base0, base1, base2))
                ),
                () -> assertEquals(
                        Arrays.asList(
                                "Some standard description, Some standard additional description",
                                "Some standard description, Only additional description has been provided",
                                "Some description, Some additional description"
                        ),
                        Service.getInfo(Arrays.asList(derived0, derived1, derived2))
                ),
                () -> assertEquals(
                        Arrays.asList(
                                "Some description",
                                "Not just a base indeed, Hey!"
                        ),
                        Service.getInfo(Arrays.asList(base1, base3))
                )
        );
    }
    
    
    @Test
    void testIsFunctionalInterface() {
        assertAll(
                () -> assertFalse(Service.isFunctionalInterface(Base.class)),
                () -> assertFalse(Service.isFunctionalInterface(Serializable.class)),
                () -> assertTrue(Service.isFunctionalInterface(Function.class)),
                () -> assertTrue(Service.isFunctionalInterface(Predicate.class)),
                () -> assertFalse(Service.isFunctionalInterface(Comparator.class))
        );
    }
    
    
    @Test
    void testImplementsSerializable() {
        assertAll(
                () -> assertTrue(Service.implementsSerializable(new Base())),
                () -> assertFalse(Service.implementsSerializable(new Derived())),
                () -> assertFalse(Service.implementsSerializable(1.1)),
                () -> assertFalse(Service.implementsSerializable(new Object())),
                () -> assertFalse(Service.implementsSerializable(new SomeExternalizableClass()))
        );
    }
    
    
    @Test
    void testIsSerializable() {
        assertAll(
                () -> assertTrue(Service.isSerializable(new Base())),
                () -> assertTrue(Service.isSerializable(new Derived())),
                () -> assertTrue(Service.isSerializable(1.1)),
                () -> assertFalse(Service.isSerializable(new Object())),
                () -> assertTrue(Service.isSerializable(new SomeExternalizableClass()))
        );
    }
    
    
    @Test
    void testGetCanonicalNamesOfClassesWithStaticMethods() {
        assertEquals(
                Arrays.asList(
                        "service.Service",
                        "java.util.Arrays",
                        "java.util.function.Predicate",
                        "org.junit.jupiter.api.Assertions"
                ),
                Service.getCanonicalNamesOfClassesWithStaticMethods(Arrays.asList(
                        Base.class,
                        Service.class,
                        Arrays.class,
                        Predicate.class,
                        Serializable.class,
                        Assertions.class,
                        Test.class,
                        TestService.class
                )));
    }
}
