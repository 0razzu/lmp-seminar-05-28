package model;


import java.util.Objects;


public class Derived extends Base {
    private String additionalDescription;
    
    
    public Derived() {
        setAdditionalDescription("Some standard additional description");
    }
    
    
    public Derived(String additionalDescription) {
        setAdditionalDescription(additionalDescription);
    }
    
    
    public Derived(String description, String additionalDescription) {
        super(description);
        setAdditionalDescription(additionalDescription);
    }
    
    
    public void setAdditionalDescription(String additionalDescription) {
        if (additionalDescription == null)
            throw new IllegalArgumentException("Description cannot be null");
        
        this.additionalDescription = additionalDescription;
    }
    
    
    public String getAdditionalDescription() {
        return additionalDescription;
    }
    
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Derived derived = (Derived) o;
        return Objects.equals(additionalDescription, derived.additionalDescription);
    }
    
    
    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), additionalDescription);
    }
}
