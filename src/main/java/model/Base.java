package model;


import java.io.Serializable;
import java.util.Objects;


public class Base implements Serializable {
    private String description;
    
    
    public Base(String description) {
        setDescription(description);
    }
    
    
    public Base() {
        this("Some standard description");
    }
    
    
    public void setDescription(String description) {
        if (description == null)
            throw new IllegalArgumentException("Description cannot be null");
        
        this.description = description;
    }
    
    
    public String getDescription() {
        return description;
    }
    
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Base base = (Base) o;
        return Objects.equals(description, base.description);
    }
    
    
    @Override
    public int hashCode() {
        return Objects.hash(description);
    }
}
