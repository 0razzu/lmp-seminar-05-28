package service;


import model.Base;
import model.Derived;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;


public class Service {
    public static List<String> getInfo(List<Base> objects) {
        List<String> info = new ArrayList<>(objects.size());
        
        for (Base e: objects)
            if (e instanceof Derived)
                info.add(e.getDescription() + ", " + ((Derived) e).getAdditionalDescription());
            else
                info.add(e.getDescription());
        
        return info;
    }
    
    
    public static boolean isFunctionalInterface(Class<?> aClass) {
        if (!aClass.isInterface())
            return false;
        
        Method[] methods = aClass.getMethods();
        Method publicAbstract = null;
        
        for (Method method: methods) {
            int modifiers = method.getModifiers();
            
            if (Modifier.isPublic(modifiers) && Modifier.isAbstract(modifiers)) {
                if (publicAbstract == null)
                    publicAbstract = method;
                
                else
                    return false;
            }
        }
        
        return publicAbstract != null;
    }
    
    
    public static boolean implementsSerializable(Object object) {
        Class<?>[] interfaces = object.getClass().getInterfaces();
        
        for (Class<?> anInterface: interfaces)
            if (anInterface == Serializable.class)
                return true;
        
        return false;
    }
    
    
    public static boolean isSerializable(Object object) {
        return object instanceof Serializable;
    }
    
    
    public static List<String> getCanonicalNamesOfClassesWithStaticMethods(List<Class<?>> classes) {
        List<String> canonicalNames = new ArrayList<>();
        
        for (Class<?> aClass: classes) {
            Method[] methods = aClass.getMethods();
            
            for (Method method: methods)
                if (Modifier.isStatic(method.getModifiers())) {
                    canonicalNames.add(aClass.getCanonicalName());
                    
                    break;
                }
        }
        
        return canonicalNames;
    }
}
